using MarketingNewsletter.Core.Entities.DistributionChanelAggregate;

namespace MarketingNewsletter.Core.Entities.RecipientAggregate
{
    public class RecipientDistributionChanel : BaseEntity
    {
        public int Id { get; set; }

        public string Value { get; set; }

        public int DistributionChanelId { get; set; }

        public DistributionChanel distributionChanel { get; set; }

        public int RecepientId { get; set; }

        public Recipient Recipient { get; set; }
    }
}