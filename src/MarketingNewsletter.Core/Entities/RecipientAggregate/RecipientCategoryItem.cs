using MarketingNewsletter.Core.Entities.CategoryAggregate;

namespace MarketingNewsletter.Core.Entities.RecipientAggregate
{
    public class RecipientCategoryItem : BaseEntity
    {
        public int RecipientId { get; set; }

        public Recipient Recipient { get; set; }
        
        public int CategoryItemId { get; set; }

        public CategoryItem CategoryItem { get; set; }
    }
}