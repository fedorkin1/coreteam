using System.Collections.Generic;
using MarketingNewsletter.Core.Interface;

namespace MarketingNewsletter.Core.Entities.RecipientAggregate
{
    public class Recipient : BaseEntity, IAggregateRoot
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public virtual ICollection<RecipientCategoryItem> CategoryItems { get; set; }

        public virtual ICollection<RecipientDistributionChanel> DistributionChanelItems { get; set; }
    }
}