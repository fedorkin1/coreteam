namespace MarketingNewsletter.Core.Entities.CategoryAggregate
{
    public class CategoryItem : BaseEntity
    {
        public int Id { get; set; }

        public int CategoryId { get; set; }

        public Category Category { get; set; }

        public string Value { get; set; }

        public bool IsNumber { get; set; }
    }
}