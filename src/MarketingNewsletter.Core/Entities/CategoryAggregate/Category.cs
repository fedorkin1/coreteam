using System.Collections.Generic;
using MarketingNewsletter.Core.Interface;

namespace MarketingNewsletter.Core.Entities.CategoryAggregate
{
    public class Category : BaseEntity, IAggregateRoot
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<CategoryItem> Items { get; set; }
    }
}