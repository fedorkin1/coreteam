using System.Collections.Generic;

namespace MarketingNewsletter.Core.Entities.CustomerAggregate.Filters
{
    public class MultipleCategoryFilter : BaseCategoryFilter
    {
        public List<int> CategoriesItems { get; set; }
    }
}