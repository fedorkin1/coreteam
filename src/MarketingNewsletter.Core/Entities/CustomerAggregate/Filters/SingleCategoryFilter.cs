namespace MarketingNewsletter.Core.Entities.CustomerAggregate.Filters
{
    public class SingleCategoryFilter : BaseCategoryFilter
    {
        public int CategoryItemId { get; set; }
    }
}