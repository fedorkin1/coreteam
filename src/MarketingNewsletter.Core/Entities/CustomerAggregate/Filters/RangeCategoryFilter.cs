namespace MarketingNewsletter.Core.Entities.CustomerAggregate.Filters
{
    public class RangeCategoryFilter : BaseCategoryFilter
    {
        public int StartCategoryItem { get; set; }

        public int EndCategoryItem { get; set; }
    }
}