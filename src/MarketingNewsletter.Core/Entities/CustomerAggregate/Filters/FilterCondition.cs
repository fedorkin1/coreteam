namespace MarketingNewsletter.Core.Entities.CustomerAggregate.Filters
{
    public enum FilterCondition
    {
        Include,

        Exclude
    }
}