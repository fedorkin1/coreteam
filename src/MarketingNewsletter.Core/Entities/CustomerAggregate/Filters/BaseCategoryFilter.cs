using MarketingNewsletter.Core.Entities.CategoryAggregate;

namespace MarketingNewsletter.Core.Entities.CustomerAggregate.Filters
{
    public abstract class BaseCategoryFilter // ValueObject
    {
        public FilterCondition Condition { get; set; }
    }
}