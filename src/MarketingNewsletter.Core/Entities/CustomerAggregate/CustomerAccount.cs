namespace MarketingNewsletter.Core.Entities.CustomerAggregate
{
    public class CustomerAccount : BaseEntity
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }

        public Customer Customer { get; set; }

        public decimal Balance { get; set; }
    }
}