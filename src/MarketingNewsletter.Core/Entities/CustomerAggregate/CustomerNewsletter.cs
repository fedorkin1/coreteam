using System.Collections.Generic;
using MarketingNewsletter.Core.Entities.CustomerAggregate.Filters;

namespace MarketingNewsletter.Core.Entities.CustomerAggregate
{
    public class CustomerNewsletter
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }

        public Customer Customer { get; set; }

        public string Text { get; set; }

        public uint RecipientsNumber { get; set; }

        public decimal Cost { get; set; }

        public List<BaseCategoryFilter> CategoryFilters { get; set; }
    }
}