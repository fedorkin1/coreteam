using System.Collections.Generic;
using MarketingNewsletter.Core.Interface;

namespace MarketingNewsletter.Core.Entities.CustomerAggregate
{
    public class Customer : BaseEntity, IAggregateRoot
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public virtual ICollection<CustomerNewsletter> Newsletters { get; set; }
        
    }
}