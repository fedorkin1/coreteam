using MarketingNewsletter.Core.Abstractions.Repositories;
using MarketingNewsletter.Core.Entities;

namespace MarketingNewsletter.Core.Abstractions.Services
{
    public class IService <T> where T: BaseEntity
    {
        protected IRepository<T> Repository { get; }
    }
}