﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MarketingNewsletter.Core.Entities;

namespace MarketingNewsletter.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(int id);

        Task CreateAsync(T value);

        Task UpdateAsync(T value);
        
        Task DeleteAsync(int id);
    }
}